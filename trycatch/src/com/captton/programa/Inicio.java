package com.captton.programa;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Inicio {

	private static Scanner scanner;

	public static void main(String[] args) {
		
		String resultado = "";
		
		int numerador, denominador;
		
		scanner = new Scanner(System.in);
		
		try {
		System.out.println("Ingrese el numerador: ");
		numerador = scanner.nextInt();
		
		System.out.println("Ingrese el denominador: ");
		denominador = scanner.nextInt();
		
		resultado = String.valueOf(numerador/denominador);
		
		System.out.println("El resultado es: "+resultado);
		
		
		}catch (ArithmeticException e) {
					
			System.out.println("No se puede dividir por 0");
		}catch (InputMismatchException e){
			System.out.println("Por favor ingrese numeros y letras");
		}
		// Para que es el FINALLY?????
		
	}

}
